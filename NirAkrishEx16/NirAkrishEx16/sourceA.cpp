#define LOL 0;
#include "sqilte3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}
int main()
{
	char *zErrMsg = 0;
	bool flag = true;
	int FirstPart;
	sqlite3* db;
	FirstPart = sqlite3_open("FirstPart.db",&db);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	sqlite3_exec(db, "drop table people", NULL, 0, 0);
	FirstPart = sqlite3_exec(db, "CREATE TABLE people(id INTEGER PRIMARY KEY AUTOINCREMENT, name STRING);", NULL, 0, &zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	FirstPart = sqlite3_exec(db, "insert into people (name) values(\"lol\")", NULL, 0, &zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	FirstPart = sqlite3_exec(db, "insert into people (name) values(\"troll\")", NULL, 0, &zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	FirstPart = sqlite3_exec(db, "insert into people (name) values(\"zzz\")", NULL, 0, &zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	FirstPart = sqlite3_exec(db, "UPDATE people SET name = \"thesecondLOOL\" WHERE name='zzz'", NULL, 0, &zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	FirstPart = sqlite3_exec(db, "SELECT * FROM people",callback,0,&zErrMsg);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	printTable();
	system("PAUSE");
	return LOL;
}