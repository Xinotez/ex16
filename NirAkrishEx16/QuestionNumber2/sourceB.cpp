#define LOL 0;
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int tempo,tempio,price,balancio,priceOfWantedCar,result;
	char* dest;
	tempo = sqlite3_exec(db, (("SELECT available FROM cars WHERE id = " + to_string(carid))).c_str(), callback, 0, &zErrMsg);
	if (tempo != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		auto iterator = results.begin();
		if (iterator->second[0]!="0")
		{
			price = sqlite3_exec(db, ("SELECT price FROM cars WHERE id = " + to_string(carid)).c_str(), callback, 0, &zErrMsg);
			if (price != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			else
			{
				iterator++;
				priceOfWantedCar = stoi(iterator->second[0]);
			}
		}
		tempio = sqlite3_exec(db, ("SELECT balance FROM accounts WHERE id = " + to_string(buyerid)).c_str(), callback, 0, &zErrMsg);
		if (tempio != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		else
		{
			iterator++;
			balancio = stoi(iterator->second[1]);
		}
		if (balancio >= priceOfWantedCar)
		{
			result = balancio - priceOfWantedCar;
			tempio = sqlite3_exec(db, ("UPDATE cars set available = 0 WHERE id = " + to_string(carid)).c_str(),NULL,0,&zErrMsg);
			if (tempio != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			tempio = sqlite3_exec(db, (("UPDATE accounts set balance = " + to_string(result) + "WHERE id = " + to_string(buyerid)).c_str()), NULL, 0, &zErrMsg);
			if (tempio != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			return true;
		}
	}
	return false;
}
int getLength(int num)
{
	int length = 1;
	while (num /= 10)
	{
		length++;
	}
	return length;
}
bool balanceTransfer(int from,int to, int amount, sqlite3* db, char* zErrMsg)
{
	int temp,ba1,ba2,na;
	temp = sqlite3_exec(db, ("SELECT balance FROM accounts WHERE id = "+to_string(from)).c_str(),callback,0,&zErrMsg);
	if (temp != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	auto iterator = results.begin();
	ba1 = stoi(iterator->second[0]);
	if (ba1 < amount)
	{
		return false;
	}
	else
	{
		temp = sqlite3_exec(db, ("SELECT balance FROM accounts WHERE id = " + to_string(to)).c_str(), callback, 0, &zErrMsg);
		if (temp != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		iterator++;
		ba2 = stoi(iterator->second[0]);
		na = ba1 - amount;
		ba1 = ba2 + amount;
		temp = sqlite3_exec(db, ("UPDATE accounts set balance = "+to_string(ba1)+" WHERE id = " + to_string(to)).c_str(), NULL, 0, &zErrMsg);
		if (temp != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		temp = sqlite3_exec(db, ("UPDATE accounts set balance = " + to_string(na) + " WHERE id = " + to_string(from)).c_str(), NULL, 0, &zErrMsg);
		if (temp != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		return true;
	}
}
int main()
{
	char *zErrMsg = 0;
	bool flag = true;
	int FirstPart;
	sqlite3* db;
	FirstPart = sqlite3_open("carsDealer.db", &db);
	if (FirstPart != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	return LOL;
}
